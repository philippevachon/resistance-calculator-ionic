// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('starter', ['mn', 'ionic'])

.config(['$ionicConfigProvider', function($ionicConfigProvider) {
    $ionicConfigProvider.tabs.position('bottom');
}])

.run(function($ionicPlatform) {
    $ionicPlatform.ready(function() {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        }
        if (window.StatusBar) {
            StatusBar.styleDefault();
        }
    });
})

.controller('CalculatorCtrl', ['$scope', 'Resistance', function($scope, Resistance) {
    var ctrl = this;

    var resistance = new Resistance();
    ctrl.resistance = resistance.getValue(['orange', 'grey', 'blue', 'gold']);
}])

.directive('resistanceCalculator', function() {
    return {
        restrict: 'E',
        scope: {
            bands: '='
        },
        controller: ['$scope', 'Resistance', 'bandColors', function($scope, Resistance, bandColors) {
            
            var ctrl = this;

            this.selectedBand = 0;
            this.bands = $scope.bands;
            this.bandsRange = _.range(0, $scope.bands);
            this.colors = [];
            this.resistance = new Resistance();
            this.bandColors = bandColors;

            this.getResistanceValue = function() {
                if(this.colors.length == this.bands) return this.resistance.getValue(this.colors);
                return null;
            }
            
            this.selectBand = function(band) {
                this.selectedBand = band;
            };

            this.selectNextBand = function() {
                this.selectedBand = (this.selectedBand < this.bands - 1) ? 
                    this.selectedBand + 1 : 0;
            };

            this.selectPreviousBand = function() {
                this.selectedBand = (this.selectedBand > 0) ? 
                    this.selectedBand - 1 : this.bands - 1;
            };

            this.setColor = function(color) {
                this.colors[this.selectedBand] = color;
            }

            this.getColor = function(band) {
                if(typeof this.colors[band] !== 'undefined') return this.colors[band];
                return '';
            }

            this.getPickerColors = function() {
                var colors = bandColors;
                if(this.selectedBand == 0) colors = _.omit(colors, 'black');
                if(this.selectedBand < this.bands - 2) colors = _.omit(colors, ['gold', 'silver']);
                if(this.selectedBand == this.bands - 2) colors = _.omit(colors, ['grey', 'white']);
                if(this.selectedBand == this.bands - 1) colors = _.pick(colors, ['brown', 'red', 'gold', 'silver']);
                return colors;
            }

            $scope.$watch(function() { return ctrl.selectedBand; }, function() {
                ctrl.bandColors = ctrl.getPickerColors();
            });
        }],
        controllerAs: 'calculator',
        templateUrl: 'templates/resistance-calculator.html'
    }
})

.directive('resistanceColorPicker', function() {
    return {
        restrict: 'E',
        require: '^resistanceCalculator',
        scope: {
            colors: '='
        },
        templateUrl: 'templates/resistance-color-picker.html',
        controller: ['$scope', function($scope) {
            $scope.bandColors = $scope.colors;
        }],
        link: function(scope, element, attr, calculator) {
            scope.pickColor = function(color) {
                console.log(color);
                calculator.setColor(color);
            }
        }
    }
})

.filter('resistance', function() {
    return function(value) {
        if(!value) return null;
        if (value >= 1000000) return (value / 1000000) + "M&#937;";
        else if (value >= 1000) return (value / 1000) + "k&#937;";
        else return value + "&#937;";
    };
})

.factory('_', ['$window', function($window) {
  return $window._; // assumes underscore has already been loaded on the page
}])

.factory('Resistance', ['bandColors', function(bandColors) {

    function Resistance() {
    }

    Resistance.prototype.getValue = function(colors) {
        var that = this;

        if(!_.isArray(colors)) {
            throw new TypeError('SetBandColors expects an array of colors.');
        }

        var unsupportedColors = _.difference(colors, _.keys(bandColors));
        if(unsupportedColors.length > 0) {
            throw new TypeError('Unrecognized color in setBandColors: ' + unsupportedColors);
        }

        if(colors.length > 5 || colors.length < 4) {
            throw new TypeError('Number of colors is invalid. Expects 4 or 5 color bands');
        }

        return _.reduce(colors, function(memo, value, index, list) {
            bandValue = bandColors[value].value;
            
            if (index > list.length - 2) {
                return memo;
            }
            else if (index == list.length - 2) {
                var value = memo * bandColors[value].multiplier;
                return (value % 1 == 0) ? value : value.toFixed(2);
            }
            return memo + bandValue + '';
        }, '');
    };

    return Resistance;
}])

.factory('bandColors', function(){
    return {
        black: {
            value: 0,
            multiplier: 1
        },
        brown: {
            value: 1,
            multiplier: 10
        },
        red: {
            value: 2,
            multiplier: 100
        },
        orange: {
            value: 3,
            multiplier: 1000
        },
        yellow: {
            value: 4,
            multiplier: 10000
        },
        green: {
            value: 5,
            multiplier: 100000
        },
        blue: {
            value: 6,
            multiplier: 1000000
        },
        violet: {
            value: 7,
            multiplier: 10000000
        },
        grey: {
            value: 8,
            multiplier: null
        },
        white: {
            value: 9,
            multiplier: null
        },
        gold: {
            value: null,
            multiplier: 0.1
        },
        silver: {
            value: null,
            multiplier: 0.01
        }
    }
});