# Resistance Calculator - Ionic #

***Still very much in development*** 

This is a simple ionic application that calculates values for 4 and 5 bands resistors. 

### Installation ###

Make sure you have followed everything from the Ionic "[Getting Started](http://ionicframework.com/getting-started/)" documentation and that you have all the dependencies already installed on your system. 

Then : 

* `npm install`
* `bower install`

And follow the Ionic documentation to test the application on the device of your choice.

### USAGE ###

* Select 4 or 5 bands in the tab at the bottom of the screen. 4 is selected by default.
* Pick color for each band by clicking on the corresponding color in the swatches.
* Swipe left or right to change bands.
* Once all bands have been filled, the resistance value will appear at the top of the screen.

### TODO ###

* Make it look good
* Write tests for Resistance.getValue
* Refactor code and separate app.js into multiple files
* Add a learning section that will train you to remember band values on resistors